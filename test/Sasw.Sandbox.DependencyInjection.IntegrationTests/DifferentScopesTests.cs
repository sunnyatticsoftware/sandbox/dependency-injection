﻿using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Sasw.TestSupport;
using System;
using Xunit;

namespace Sasw.Sandbox.DependencyInjection.IntegrationTests
{
    public static class DifferentScopesTests
    {
        private static readonly ServiceProviderOptions ServiceProviderOptions =
            new ServiceProviderOptions
            {
                ValidateOnBuild = false,
                ValidateScopes = true
            };

        public class Given_Singleton_Repository_And_Singleton_Service_That_Uses_The_Repository_When_Getting_Service_From_Different_Scope
            : Given_When_Then_Test
        {
            private IServiceScope _scopeOne;
            private IServiceScope _scopeTwo;
            private ServiceSample _serviceSampleOne;
            private ServiceSample _serviceSampleTwo;

            protected override void Given()
            {
                var serviceCollection =
                    new ServiceCollection()
                        .AddSingleton<RepositorySample>()
                        .AddSingleton<ServiceSample>()
                        .BuildServiceProvider(ServiceProviderOptions);

                _scopeOne = serviceCollection.CreateScope();
                _scopeTwo = serviceCollection.CreateScope();

                _serviceSampleOne = _scopeOne.ServiceProvider.GetService<ServiceSample>();
            }

            protected override void When()
            {
                _serviceSampleTwo = _scopeTwo.ServiceProvider.GetService<ServiceSample>();
            }

            [Fact]
            public void Then_It_Should_Get_The_Same_Service_Instance()
            {
                _serviceSampleOne.Should().Be(_serviceSampleTwo);
            }

            [Fact]
            public void Then_It_Should_Have_The_Same_Repository_Instance()
            {
                _serviceSampleOne.RepositorySample.Should().Be(_serviceSampleTwo.RepositorySample);
            }
        }

        public class Given_Transient_Repository_And_Transient_Service_That_Uses_The_Repository_When_Getting_Service_From_Different_Scope
            : Given_When_Then_Test
        {
            private IServiceScope _scopeOne;
            private IServiceScope _scopeTwo;
            private ServiceSample _serviceSampleOne;
            private ServiceSample _serviceSampleTwo;

            protected override void Given()
            {
                var serviceCollection =
                    new ServiceCollection()
                        .AddTransient<RepositorySample>()
                        .AddTransient<ServiceSample>()
                        .BuildServiceProvider(ServiceProviderOptions);

                _scopeOne = serviceCollection.CreateScope();
                _scopeTwo = serviceCollection.CreateScope();

                _serviceSampleOne = _scopeOne.ServiceProvider.GetService<ServiceSample>();
            }

            protected override void When()
            {
                _serviceSampleTwo = _scopeTwo.ServiceProvider.GetService<ServiceSample>();
            }

            [Fact]
            public void Then_It_Should_Get_Different_Service_Instance()
            {
                _serviceSampleOne.Should().NotBe(_serviceSampleTwo);
            }

            [Fact]
            public void Then_It_Should_Have_A_Repository_Instance()
            {
                _serviceSampleOne.RepositorySample.Should().NotBe(_serviceSampleTwo.RepositorySample);
            }
        }

        public class Given_Scoped_Repository_And_Scoped_Service_That_Uses_The_Repository_When_Getting_Service_From_Different_Scope
            : Given_When_Then_Test
        {
            private IServiceScope _scopeOne;
            private IServiceScope _scopeTwo;
            private ServiceSample _serviceSampleOne;
            private ServiceSample _serviceSampleTwo;

            protected override void Given()
            {
                var serviceCollection =
                    new ServiceCollection()
                        .AddScoped<RepositorySample>()
                        .AddScoped<ServiceSample>()
                        .BuildServiceProvider(ServiceProviderOptions);

                _scopeOne = serviceCollection.CreateScope();
                _scopeTwo = serviceCollection.CreateScope();

                _serviceSampleOne = _scopeOne.ServiceProvider.GetService<ServiceSample>();
            }

            protected override void When()
            {
                _serviceSampleTwo = _scopeTwo.ServiceProvider.GetService<ServiceSample>();
            }

            [Fact]
            public void Then_It_Should_Get_Different_Service_Instance()
            {
                _serviceSampleOne.Should().NotBe(_serviceSampleTwo);
            }

            [Fact]
            public void Then_It_Should_Have_A_Repository_Instance()
            {
                _serviceSampleOne.RepositorySample.Should().NotBe(_serviceSampleTwo.RepositorySample);
            }
        }

        public class Given_Scoped_Repository_And_Transient_Service_That_Uses_The_Repository_When_Getting_Service_From_Different_Scope
            : Given_When_Then_Test
        {
            private IServiceScope _scopeOne;
            private IServiceScope _scopeTwo;
            private ServiceSample _serviceSampleOne;
            private ServiceSample _serviceSampleTwo;

            protected override void Given()
            {
                var serviceCollection =
                    new ServiceCollection()
                        .AddScoped<RepositorySample>()
                        .AddTransient<ServiceSample>()
                        .BuildServiceProvider(ServiceProviderOptions);

                _scopeOne = serviceCollection.CreateScope();
                _scopeTwo = serviceCollection.CreateScope();

                _serviceSampleOne = _scopeOne.ServiceProvider.GetService<ServiceSample>();
            }

            protected override void When()
            {
                _serviceSampleTwo = _scopeTwo.ServiceProvider.GetService<ServiceSample>();
            }

            [Fact]
            public void Then_It_Should_Get_Different_Service_Instance()
            {
                _serviceSampleOne.Should().NotBe(_serviceSampleTwo);
            }

            [Fact]
            public void Then_It_Should_Have_A_Repository_Instance()
            {
                _serviceSampleOne.RepositorySample.Should().NotBe(_serviceSampleTwo.RepositorySample);
            }
        }

        public class Given_Scoped_Repository_And_Singleton_Service_That_Uses_The_Repository_When_Getting_The_Service_From_Scope
            : Given_When_Then_Test
        {
            private IServiceScope _scopeOne;
            private ServiceSample _serviceSampleOne;
            private InvalidOperationException _exception;

            protected override void Given()
            {
                var serviceCollection =
                    new ServiceCollection()
                        .AddScoped<RepositorySample>()
                        .AddSingleton<ServiceSample>()
                        .BuildServiceProvider(ServiceProviderOptions);

                _scopeOne = serviceCollection.CreateScope();
            }

            protected override void When()
            {
                try
                {
                    _serviceSampleOne = _scopeOne.ServiceProvider.GetService<ServiceSample>();
                }
                catch (InvalidOperationException exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Throw_An_InvalidOperationException()
            {
                _exception.Should().NotBeNull();
            }
        }

        class RepositorySample : IDisposable
        {
            public void Dispose()
            {
                throw new NotImplementedException();
            }
        }
        class ServiceSample
        {
            public RepositorySample RepositorySample { get; }

            public ServiceSample(RepositorySample repositorySample)
            {
                RepositorySample = repositorySample;
            }
        }
    }
}